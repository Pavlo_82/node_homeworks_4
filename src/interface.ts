import { Transaction } from 'Transaction.ts';
import { CurrencyEnum } from 'CurrencyEnum.ts';
export interface ICard {
  AddTransaction(transaction: Transaction): string;
  AddTransaction(amount: number, currency: CurrencyEnum): string;
  GetTransaction(id: string): Transaction | null;
  GetBalance(currency: CurrencyEnum): number;
}
