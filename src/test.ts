import Card from 'Card.ts';
import { BonusCard } from 'BonusCard.ts';
import { Pocket } from 'Pocket.ts';
import { CurrencyEnum } from './CurrencyEnum.ts';

const card1 = new Card();
const card2 = new BonusCard();

const pocket = new Pocket();

card1.AddTransaction(100, CurrencyEnum.USD);
card1.AddTransaction(300, CurrencyEnum.UAH);

card2.AddTransaction(200, CurrencyEnum.USD);

pocket.AddCard('card1', card1);
pocket.AddCard('card2', card2);

console.log(pocket.GetTotalAmount(CurrencyEnum.USD)); // Should print 300
console.log(pocket.GetTotalAmount(CurrencyEnum.UAH)); // Should print 300
