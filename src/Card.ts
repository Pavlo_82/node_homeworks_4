import { Transaction } from './Transaction.ts';
import { CurrencyEnum } from './CurrencyEnum.ts';
import { ICard } from './interface.ts';

class Card implements ICard {
  private transactions: Transaction[] = [];

  // Overload signatures
  AddTransaction(transaction: Transaction): string;
  AddTransaction(amount: number, currency: CurrencyEnum): string;

  // Implementation signature
  AddTransaction(
    transactionOrAmount: Transaction | number,
    currency?: CurrencyEnum,
  ): string {
    if (transactionOrAmount instanceof Transaction) {
      this.transactions.push(transactionOrAmount);
      return transactionOrAmount.Id;
    } else if (typeof transactionOrAmount === 'number') {
      if (currency === undefined) {
        throw new Error(
          'Currency must be provided when the amount is a number.',
        );
      }
      const transaction = new Transaction(transactionOrAmount, currency);
      this.transactions.push(transaction);
      return transaction.Id;
    }
    throw new Error('Invalid arguments');
  }

  GetTransaction(id: string): Transaction | null {
    return this.transactions.find(transaction => transaction.Id === id) || null;
  }

  GetBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter(transaction => transaction.Currency === currency)
      .reduce((acc, transaction) => acc + transaction.Amount, 0);
  }
}

export default Card;
