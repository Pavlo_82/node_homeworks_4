import { Transaction } from './Transaction.ts';
import { CurrencyEnum } from './CurrencyEnum.ts';
import { ICard } from './interface.ts';
import Card from 'Card.ts';

export class BonusCard extends Card implements ICard {
  GetTransaction(id: string): Transaction | null {
    throw new Error('Method not implemented.');
  }
  GetBalance(currency: CurrencyEnum): number {
    throw new Error('Method not implemented.');
  }
  AddTransaction(
    transactionOrAmount: Transaction | number,
    currency?: CurrencyEnum,
  ): string {
    if (typeof transactionOrAmount === 'number' && currency) {
      const id = super.AddTransaction(transactionOrAmount, currency);
      const bonusTransaction = new Transaction(
        transactionOrAmount * 0.1,
        currency,
      );
      this.AddTransaction(bonusTransaction);
      return id;
    } else if (transactionOrAmount instanceof Transaction) {
      return super.AddTransaction(transactionOrAmount);
    }
    throw new Error('Invalid arguments');
  }
}
