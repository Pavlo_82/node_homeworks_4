import { ICard } from './interface.ts';
import { CurrencyEnum } from './CurrencyEnum.ts';

export class Pocket {
  private cards: Record<string, ICard> = {};

  AddCard(name: string, card: ICard): void {
    this.cards[name] = card;
  }

  RemoveCard(name: string): void {
    delete this.cards[name];
  }

  GetCard(name: string): ICard | undefined {
    return this.cards[name];
  }

  GetTotalAmount(currency: CurrencyEnum): number {
    return Object.values(this.cards).reduce(
      (total, card) => total + card.GetBalance(currency),
      0,
    );
  }
}
